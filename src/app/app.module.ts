import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, Provider } from '@angular/core';


import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { AuthModule } from './modules/auth/auth.module';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app.routing.module';
import { HomeModule } from './modules/home/home.module';
import { AuthService } from './core/service/auth.service';
import { AuthGuard } from './core/guard/auth.guard';

/*
Firebase
*/

import { RouterModule } from '@angular/router';
import { SidebarInfoComponent } from './layout/sidebar/sidebar-info/sidebar-info.component';
import { SidebarMenuComponent } from './layout/sidebar/sidebar-menu/sidebar-menu.component';
import { HeaderComponent } from './layout/header/header.component';
import { ContentLayoutComponent } from './layout/content-layout/content-layout.component';
import { ChangeViewComponent } from './layout/header/change-view/change-view.component';
import { SearchComponent } from './layout/header/search/search.component';
import { FilterComponent } from './layout/header/filter/filter.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { ThemeSwitcherComponent } from './layout/sidebar/sidebar-menu/theme-switcher/theme-switcher.component';
import { UserComponent } from './layout/sidebar/sidebar-menu/user/user.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInerceptor } from './shared/interceptors/auth.interceptor';
import { NoAuthGuard } from './core/guard/no-auth.guard';
import { MoviesNewComponent } from './movies/movies-new/movies-new.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

const INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: AuthInerceptor
}


@NgModule({
  declarations: [
    AppComponent,
    SidebarInfoComponent,
    SidebarMenuComponent,
    HeaderComponent,  
    ChangeViewComponent,
    SearchComponent, 
    FilterComponent, 
    SidebarComponent,
    ThemeSwitcherComponent,
    SidebarComponent, 
    UserComponent,
    ContentLayoutComponent,
    MoviesNewComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    HomeModule, 
    RouterModule.forRoot([]),
    CoreModule,
    AuthModule,
    ReactiveFormsModule,
    FormsModule,
    NgxMaskModule.forRoot()
  ],
  providers: [
    AuthService,
    AuthGuard,
    NoAuthGuard,
    INTERCEPTOR_PROVIDER
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }

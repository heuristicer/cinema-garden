import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class NoAuthGuard implements CanActivate {

 constructor(
   public authService: AuthService,
   public router: Router
 ) {}

 canActivate(
  next: ActivatedRouteSnapshot,
  state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.authService.isLogged()) {
      this.router.navigate(['home'], {
        queryParams: {
          loginAgain: true
        }
      })
    } else {
      return true
      
    }

}
}

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth'
// import {auth} from ''
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from 'src/app/shared/interfaces/interfaces';
import { tap, catchError } from 'rxjs/operators';
import { Observable, throwError, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthResponse } from 'src/app/shared/interfaces/interfaces';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';


@Injectable()
export class AuthService {
  userData: any;

  public error$: Subject<string> = new Subject<string>()

  constructor(
    public afs: AngularFirestore, // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    public snackService: SnackbarService,
    private http: HttpClient 
  ) {
  }

  get token(): string {
    const expDate = new Date(localStorage.getItem('fb-token-exp'))
    if(new Date() > expDate) {
      this.signOut()
      return null
    }
    return localStorage.getItem('fb-token')
  }

  signIn(user: User): Observable<any> {
    user.returnSecureToken = true
    return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.firebase.apiKey}`, user)
      .pipe(
        tap(this.setToken),
        catchError(this.handleError.bind(this))
      )
  }

  

    signUp(email, password) {
      return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
        .then((result)=> {
          console.log('123');
        }).catch((error)=>{
          window.alert(error.message)
        }) 
    }




  signOut() {
    this.setToken(null)
  }

  isLogged():boolean {
    return !!this.token
  }

  private handleError(error: HttpErrorResponse) {
    const {message} = error.error.error

    switch (message) {
      case 'INVALID_EMAIL':
        this.error$.next('Invalid Email!')
        break
      case 'INVALID_PASSWORD':
        this.error$.next('Invalid Password!')
        break
      case 'EMAIL_NOT_FOUND':
        this.error$.next('This email in not found!')
        break
    }

    return throwError(error)
  }

  private setToken(response: AuthResponse | null) { 
    console.log(response)
    if(response) {
      const expDate = new Date(new Date().getTime() + +response.expiresIn * 1000)
      localStorage.setItem('fb-token', response.idToken)
      localStorage.setItem('fb-token-exp', expDate.toString())
    } else {
      localStorage.clear()
    }
  }

}

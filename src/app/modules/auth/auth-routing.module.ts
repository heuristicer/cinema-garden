import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './page/login/login.component';
import { RegisterComponent } from './page/register/register.component';
import { VerifyEmailComponent } from './page/verify-email/verify-email.component';
import { AuthGuard } from '../../core/guard/auth.guard';
import { AuthService } from 'src/app/core/service/auth.service';
import { AuthComponent } from './page/auth/auth.component';



const routes: Routes = [
  {
    path: '', component: AuthComponent, children: [
      {path: '', component: LoginComponent},
      { path: '', redirectTo: '/', pathMatch: 'full' },
      { path: 'register', component: RegisterComponent },
      { path: 'verify-email-address', component: VerifyEmailComponent }
    ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [
    RouterModule
  ],
  providers: [
    AuthService,
    AuthGuard
  ]
})
export class AuthRoutingModule { } 
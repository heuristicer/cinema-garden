import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/shared/interfaces/interfaces';
import { Router } from '@angular/router';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  registred = false;
 

  constructor(
    public authService: AuthService,
    public fb: FormBuilder,
    private router: Router,
    private snackService: SnackbarService
  ) { }



  ngOnInit() {

    this.registerForm = this.fb.group({
      userEmail: ['', [Validators.required]],
      userPassword: ['', [Validators.required, Validators.minLength(6)]]
    })

  }

  submit() {
    if(this.registerForm.invalid) {
      return
    }

    this.authService.signUp(this.registerForm.value.userEmail, this.registerForm.value.userPassword).then((result)=>{
      this.registerForm.reset()
      this.registred = false
      this.snackService.openSnackBar('You have successfully registered! You will be redirect to Login Page ')
      setTimeout(()=>{
        this.router.navigate(['auth'])
        this.snackService.dismiss()
      }, 5000)
    })


  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/core/service/auth.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from 'src/app/shared/interfaces/interfaces';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submited = false;
  message: string;

  constructor(
    public authService: AuthService, 
    private router: Router,
    private route: ActivatedRoute,
    private snackService: SnackbarService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params)=>{
        if(params['loginAgain']) {
          this.snackService.openSnackBar('Something Wrong')
        }
        else if(params['authFailed']) {
          this.snackService.openSnackBar('The session is expired. Enter the data again!')
        }
    })

    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  submit() {
    console.log(this.loginForm)
    if(this.loginForm.invalid) {
      return
    }

    this.submited = true

    const user: User = { 
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    }

    this.authService.signIn(user).subscribe(()=>{
      this.loginForm.reset()
      this.submited = false
      this.router.navigate(['home'])
    }, ()=>{
      this.submited = false 
    })

  }



}

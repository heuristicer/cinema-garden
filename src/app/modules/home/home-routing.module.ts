import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {HomeComponent} from './page/home.component';
// import { MoviesNewComponent } from 'src/app/movies/movies-new/movies-new.component';
import { EditMovieComponent } from 'src/app/movies/edit-movie/edit-movie.component';
import { AuthGuard } from 'src/app/core/guard/auth.guard';
import { MoviesListComponent } from 'src/app/movies/movies-list/movies-list.component';



const homeRoutes: Routes = [
  { path: '', component: HomeComponent, children: [
    {path: '', redirectTo: '/', pathMatch: 'full'},
    {path: '', component: MoviesListComponent},
    {path: 'movie/:id/edit', component: EditMovieComponent},
    // {path: 'movies-new', component: MoviesNewComponent, canActivate: [AuthGuard]} 
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(homeRoutes)], 
  exports: [RouterModule],
  providers: [AuthGuard] 
})
export class HomeRoutingModule { 
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './page/home.component'; 
import { MaterialModule } from 'src/app/shared/material.module';
import { FlexLayoutModule } from "@angular/flex-layout";
import { MoviesListItemComponent } from 'src/app/movies/movies-list/movies-list-item/movies-list-item.component';
import { NumericDirective } from 'src/app/shared/directives/only-number.directive';
import { EditMovieComponent } from 'src/app/movies/edit-movie/edit-movie.component';
import { MoviesListComponent } from 'src/app/movies/movies-list/movies-list.component';
// import { MoviesNewComponent } from 'src/app/movies/movies-new/movies-new.component';
import {NgxMaskModule} from 'ngx-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
    declarations: [    
        HomeComponent,
        MoviesListItemComponent, 
        NumericDirective,
        EditMovieComponent,
        MoviesListComponent,
        // MoviesNewComponent
    ],
    imports: [
        HomeRoutingModule,
        CommonModule,
        MaterialModule,
        FlexLayoutModule,
        NgxMaskModule.forRoot(),
        ReactiveFormsModule,
        FormsModule
 
    ],
    exports: [
        HomeComponent,
        MoviesListItemComponent
    ],
    providers: [],
    entryComponents: [] 
})
export class HomeModule {} 
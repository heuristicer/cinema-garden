import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { MoviesService } from 'src/app/shared/services/movies.service';
import { switchMap } from 'rxjs/operators';
import { Movie } from 'src/app/shared/models/movie.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.scss']
})
export class EditMovieComponent implements OnInit {

  editForm: FormGroup
  movie: Movie
  submitted = false
  updateSubscription: Subscription

  constructor(
    private route: ActivatedRoute,
    private moviesService: MoviesService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.route.params.pipe(
      switchMap((params: Params) => {
        return this.moviesService.getMovie(params['id'])
      })
    ).subscribe((movie: Movie) => {
      this.movie = movie
      this.editForm = this.fb.group({
        movieTitle: [movie.movieTitle, [Validators.required]],
        movieImage: [movie.movieImage, [Validators.required]],
        movieRate: [movie.movieRate, [Validators.required]],
        movieYear: [movie.movieYear, [Validators.required]],
        movieGenres: [movie.movieGenres, [Validators.required]],
        movieLength: [movie.movieLength, [Validators.required]],
        movieReview: [movie.movieReview, []]
      })
    })
  }

  ngOnDestroy() {
    if(this.updateSubscription) {
      this.updateSubscription.unsubscribe()
    }
    
  }

  submit() {
    if(this.editForm.invalid) {
      return
    }

    this.submitted = true

    this.updateSubscription = this.moviesService.updateMovie({
      id: this.movie.id,
      movieTitle: this.editForm.value.movieTitle,
      movieImage: this.editForm.value.movieImage,
      movieRate: this.editForm.value.movieRate,
      movieYear: this.editForm.value.movieYear,
      movieGenres: this.editForm.value.movieGenres,
      movieLength: this.editForm.value.movieLength,
      movieReview: this.editForm.value.movieReview
    }).subscribe(()=>{
      this.submitted = false
    })

  }

}

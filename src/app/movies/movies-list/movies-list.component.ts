import { Component, OnInit, OnDestroy } from '@angular/core';
import { Movie } from 'src/app/shared/models/movie.model'
import { MoviesService } from 'src/app/shared/services/movies.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit, OnDestroy {

  movies: Movie[] = [];
  moviesSubsciption: Subscription
  deleteSubscription: Subscription

  constructor(private moviesService: MoviesService) { }

  ngOnInit() {
    this.moviesSubsciption = this.moviesService.getMovies().subscribe(movies => {
      this.movies = movies
    })
  }


  remove(id: string) {
    this.deleteSubscription = this.moviesService.removeMovie(id).subscribe(() => {
      this.movies = this.movies.filter(movie => movie.id !== id)
    })
  }

  ngOnDestroy() {
    if (this.moviesSubsciption) {
      this.moviesSubsciption.unsubscribe()
    }

    if (this.deleteSubscription) {
      this.deleteSubscription.unsubscribe()
    }

  }
}

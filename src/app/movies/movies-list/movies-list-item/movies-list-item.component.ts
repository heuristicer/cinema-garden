import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Movie } from 'src/app/shared/models/movie.model'

@Component({
  selector: 'app-movies-list-item',
  templateUrl: './movies-list-item.component.html',
  styleUrls: ['./movies-list-item.component.scss']
})
export class MoviesListItemComponent implements OnInit {

  @Input() m: Movie
  @Output() someEvent = new EventEmitter<string>();

  remove(id:string) {
    this.someEvent.emit(id)
  }


  constructor() { } 

  ngOnInit() { }


}

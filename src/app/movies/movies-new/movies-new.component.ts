import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Movie } from 'src/app/shared/models/movie.model';
import { MoviesService } from 'src/app/shared/services/movies.service';

@Component({
  selector: 'app-movies-new',
  templateUrl: './movies-new.component.html',
  styleUrls: ['./movies-new.component.scss']
})
export class MoviesNewComponent implements OnInit {

  addingForm: FormGroup;


  constructor(
    private fb: FormBuilder,
    private movieService: MoviesService
  ) {}

  ngOnInit() {
    this.addingForm = this.fb.group({
      movieTitle: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]],
      movieImage: ['', [Validators.required]],
      movieRate: ['', [Validators.required]],
      movieYear: ['', [Validators.required]],
      movieGenres: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]],
      movieLength: ['', [Validators.required]],
      movieReview: ['', []]      
    })
  }

  submit() {
    if(this.addingForm.invalid) {
      return
    }
    const movie: Movie = {
      movieTitle: this.addingForm.value.movieTitle,
      movieImage: this.addingForm.value.movieImage,
      movieRate: this.addingForm.value.movieRate,
      movieGenres: this.addingForm.value.movieGenres,
      movieLength: this.addingForm.value.movieLength,
      movieYear: this.addingForm.value.movieYear,
      movieReview: this.addingForm.value.movieReview,

    } 
    
    this.movieService.addMovie(movie).subscribe(()=>{
      this.addingForm.reset()
    })

    console.log(movie)

  }

}

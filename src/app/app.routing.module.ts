import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './core/guard/auth.guard';
import { NoAuthGuard } from './core/guard/no-auth.guard';
import { MoviesNewComponent } from './movies/movies-new/movies-new.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full',
    canActivate: [NoAuthGuard]
  },
  { 
    path: 'auth', loadChildren: './modules/auth/auth.module#AuthModule',
    canActivate: [NoAuthGuard],
    data: { showHeader: false, showSidebar: false }
  },
  { 
    path: 'home', 
    loadChildren: './modules/home/home.module#HomeModule', 
    canActivate: [AuthGuard],
    data: { showHeader: true, showSidebar: true }
  },

  {
    path: 'movies-new',
    component: MoviesNewComponent,
    canActivate: [AuthGuard]
  },
    
  { path: '',   
    redirectTo: 'auth', 
    pathMatch: 'full',
    canActivate: [NoAuthGuard],
  },
  { path: '**', 
    redirectTo: 'auth',
    canActivate: [NoAuthGuard]
  }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }  

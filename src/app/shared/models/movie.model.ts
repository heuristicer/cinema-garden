export class Movie {
    constructor(
        public movieImage: string,
        public movieTitle: string,
        public movieRate: number,
        public movieYear: number, 
        public movieGenres: string[],
        public movieLength: string,
        public id?: string,
        public movieReview?: string,
        public imdbRaiting?: number,
        public trailer?: string
    ) { }
}

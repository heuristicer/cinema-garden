export interface AuthResponse {
    idToken: string
    expiresIn: string
}

export interface User {
    email: string
    password: string
    returnSecureToken?: boolean 
}

export interface Movie {
    imagePath: string,
    title: string,
    year: number, 
    myRaiting: number,
    genres: string[],
    length: string,
    id?: string,
    review?: string,
    imdbRaiting?: number,
    trailer?: string
}

export interface FbCreateResponse { 
    name: string
  }
  
  
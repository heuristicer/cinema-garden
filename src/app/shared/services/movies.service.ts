import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Movie } from '../models/movie.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { FbCreateResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor( private http: HttpClient ) { }

  addMovie(movie: Movie): Observable<Movie> {
    return this.http.post(`${environment.firebase.databaseURL}/movies.json`, movie)  
      .pipe(map((response: FbCreateResponse)=>{
        return {
          ...movie,
          id: response.name 
        }
      }))  
  }

  getMovies() {
    return this.http.get(`${environment.firebase.databaseURL}/movies.json`)
      .pipe(map((response: {[key: string]: any})=>{
       return  Object.keys(response).map(key=>({
         ...response[key],
         id: key
       }))
      }))
    
  }

  removeMovie(id: string): Observable<void> {
    return this.http.delete<void>(`${environment.firebase.databaseURL}/movies/${id}.json`) 
  }


  getMovie(id:string): Observable<Movie> {
    return this.http.get<Movie>(`${environment.firebase.databaseURL}/movies/${id}.json`)
    .pipe(map((movie: Movie)=>{
      return {
        ...movie,
        id
      }
    }))  
  }

  updateMovie(movie: Movie): Observable<Movie> {
    return this.http.patch<Movie>(`${environment.firebase.databaseURL}/movies/${movie.id}.json`, movie)
  }

} 

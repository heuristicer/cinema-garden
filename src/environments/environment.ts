// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDVV4Pm_wfRoRM6K0agMei62qXiJ0siTVE",
    authDomain: "cinema-garden.firebaseapp.com",
    databaseURL: "https://cinema-garden.firebaseio.com/",
    projectId: "cinema-garden",
    storageBucket: "cinema-garden.appspot.com",
    messagingSenderId: "999799245606",
    appId: "1:999799245606:web:015f5746b57de19ea58fe7",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
